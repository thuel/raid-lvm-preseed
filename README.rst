================
raid-lvm-preseed
================

In this repository I am creating a preseed file for a complex raid-lvm-setup. I will go step by step to explore the �magic� of the preseed file synthax.

The master plan is as follows:
- Take the debian known good example; let the thing run on a vm.
- Take an example with raid and lvm; let thing run on a vm.
- Extend the example to 4 devices; let thing run on a vm.

Usage
=====
The idea is to point the debian installer to the url of the config file in this repository.
